use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::application::Application;
use crate::config::{APP_ID, PROFILE};

mod imp {
    use super::*;

    #[derive(Debug, gtk::CompositeTemplate)]
    #[template(resource = "/app/drey/Ledger/ui/window.ui")]
    pub struct Window {
        #[template_child]
        pub list_view: TemplateChild<gtk::ListView>,

        pub model: gio::ListStore,
        pub settings: gio::Settings,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                settings: gio::Settings::new(APP_ID),
                list_view: TemplateChild::default(),
                model: gio::ListStore::new::<crate::Entry>(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();

            let factory = gtk::SignalListItemFactory::new();

            factory.connect_setup(|_, item| {
                let item = item.downcast_ref::<gtk::ListItem>().unwrap();
                let row = gtk::Label::new(None);
                row.set_xalign(0.0);
                item.set_child(Some(&row));
            });

            factory.connect_bind(|_, item| {
                let item = item.downcast_ref::<gtk::ListItem>().unwrap();
                let child = item.child().unwrap();
                let row = child.downcast_ref::<gtk::Label>().unwrap();

                let item = item.item().unwrap();
                let entry = item.downcast_ref::<crate::Entry>().unwrap();
                row.set_label(&entry.message());
            });
            self.list_view.set_factory(Some(&factory));
            let selection = gtk::NoSelection::new(Some(self.model.clone()));
            self.list_view.set_model(Some(&selection));

            obj.load_journal();
        }

        fn dispose(&self) {
            self.dispose_template();
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        // Save window state on delete event
        fn close_request(&self) -> glib::Propagation {
            if let Err(err) = self.obj().save_window_size() {
                tracing::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        glib::Object::builder().property("application", app).build()
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    fn load_journal(&self) {
        use systemd::{journal, Journal};
        let mut journal = journal::OpenOptions::default()
            .current_user(true)
            .open()
            .unwrap();
        let mut c = 0;
        let test = journal.next_entry().unwrap().unwrap();
        let timestamp = journal.timestamp().unwrap();
        let cursor = journal.cursor().unwrap();
        let s = timestamp
            .duration_since(std::time::SystemTime::UNIX_EPOCH)
            .unwrap();
        // let datetime = glib::DateTime::from_unix_utc(s.as_secs() as i64).unwrap();
        let datetime = glib::DateTime::from_unix_local(s.as_secs() as i64).unwrap();
        let message = journal.get_data("MESSAGE").unwrap().unwrap();
        let data = String::from_utf8(message.data().to_vec()).unwrap();
        let name = String::from_utf8(message.name().to_vec()).unwrap();
        let value = String::from_utf8(message.value().unwrap().to_vec()).unwrap();

        tracing::error!("data: {data}\nname {name}\nvalue :{value}");
        tracing::error!(
            "{}/{}/{} {}:{}",
            datetime.day_of_month(),
            datetime.month(),
            datetime.year(),
            datetime.hour(),
            datetime.minute()
        );
        tracing::error!("cursor {cursor}");
        tracing::debug!("{test:#?}");
        while let Ok(Some(item)) = journal.next_entry() {
            let entry = crate::Entry::new(item);
            let (timestamp, boot) = journal.monotonic_timestamp().unwrap();
            self.imp().model.append(&entry);
            c += 1;
            if c > 10000 {
                break;
            }
        }
    }
}
