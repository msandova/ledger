use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use systemd::journal::JournalRecord;

mod imp {
    use super::*;

    use glib::Properties;
    use std::cell::Cell;
    use std::cell::OnceCell;

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::Entry)]
    pub struct Entry {
        #[property(get = Self::message)]
        message: String, // TODO Use PhantomData

        pub inner: OnceCell<JournalRecord>,
    }

    impl Entry {
        fn inner(&self) -> &JournalRecord {
            self.inner.get().unwrap()
        }

        fn message(&self) -> String {
            self.inner().get("MESSAGE").cloned().unwrap_or("".into())
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Entry {
        const NAME: &'static str = "Entry";
        type Type = super::Entry;
        type ParentType = glib::Object;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Entry {}
}

glib::wrapper! {
    pub struct Entry(ObjectSubclass<imp::Entry>);
}

impl Entry {
    pub fn new(record: JournalRecord) -> Self {
        let obj = glib::Object::new::<Self>();
        obj.imp().inner.set(record).unwrap();
        obj
    }
}
